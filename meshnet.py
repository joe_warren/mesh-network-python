#!/usr/bin/env python3
import math
import random
from scipy.spatial import Delaunay
from itertools import combinations
import itertools
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import minimum_spanning_tree

CANVAS = [2048, 1600]

def pointDistance(a, b):
    return math.sqrt(math.pow(a[0]-b[0],2) + math.pow(a[1]-b[1],2))

def generateSemiRandomPoints( n, eps ):
    points = []
    def newPoint():
        p = list(map( lambda x: random.randrange(eps, x-eps), CANVAS ))
        if any( map( lambda x: pointDistance(p, x) < eps, points ) ):
             return newPoint()
        else:
             return p
    for i in range( 0, n ):
        points.append(newPoint())
    return points

def yieldPoints( points ):
    for p in points: 
        yield """<circle cx="%d" cy="%d" r="10"
          fill="white" stroke="black" stroke-width="3"/>""" % (p[0], p[1])

def yieldEdges( points, edges ):
    for e in edges:
        start = points[e[0]]
        end = points[e[1]]
        yield """<line x1="%d" y1="%d" 
          x2="%d" y2="%d" 
          stroke="black" 
          stroke-width="3"/>""" % (start[0], start[1], end[0], end[1])

def generateSvg( points, edges ):
    yield """<?xml version="1.0" standalone="yes"?>"""
    yield """<svg xmlns="http://www.w3.org/2000/svg" """
    yield """ width="%d" height="%d">""" % (CANVAS[0], CANVAS[1])
    yield from yieldEdges(points, edges)
    yield from yieldPoints(points)
    yield "</svg>"

def delaunayEdges( points ):
    d = Delaunay(points).simplices
    faceEdges = map( lambda x: list(combinations(x, 2)), d )
    edges = set(
        map( lambda x: (x[0], x[1]) if (x[0]<x[1]) else (x[1], x[0]), 
            itertools.chain(*faceEdges))
    )
    return edges

def minSpanEdges( points, edges, maxSpan = False ):
    size = len(points)
    m = np.zeros([size, size])
    for e in edges:
        start = points[e[0]]
        end = points[e[1]]
        length = pointDistance( start, end )
        if maxSpan:
             length = -length 
        m[e[0]][e[1]] = length
        m[e[1]][e[0]] = length
    X = csr_matrix(m)
    T = minimum_spanning_tree(X).toarray()

    for x in range(0, size):
        for y in range(x, size):
            if(T[x][y] != 0 or T[y][x] != 0):
                 yield (x, y)

def diffEdges( edges, difference ):
    return [ e for e in edges if e not in set(difference) ]
    
points = generateSemiRandomPoints(500, 30)
edges = delaunayEdges(points)

with open("delauney.svg", "w") as f:
    for line in generateSvg(points, edges):
        f.write(line)

minEdges = list(minSpanEdges(points, edges))

with open("minspan.svg", "w") as f:
    for line in generateSvg(points, minEdges):
        f.write(line)

maxEdges = list(minSpanEdges(points, edges, True))

with open("maxspan.svg", "w") as f:
    for line in generateSvg(points, maxEdges):
        f.write(line)

nonMaxEdges = diffEdges(edges, maxEdges)

with open("nonmaxspan.svg", "w") as f:
    for line in generateSvg(points, nonMaxEdges):
        f.write(line)


nonMaxMinEdges = diffEdges(nonMaxEdges, minEdges)

with open("nonmaxminspan.svg", "w") as f:
    for line in generateSvg(points, nonMaxMinEdges):
        f.write(line)

maxMinEdges = diffEdges(edges, nonMaxMinEdges)

with open("maxminspan.svg", "w") as f:
    for line in generateSvg(points, maxMinEdges):
        f.write(line)

         

